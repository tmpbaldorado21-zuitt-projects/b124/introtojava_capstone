package com.baldorado;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner tuneMovie = new Scanner(System.in);
        HashMap<String, String> tuneMovieList = new HashMap<String, String>();

        System.out.println("** Tune Movie **");
        System.out.println("[Genres: Action. Anime, Asian, Children & Family, Comedies, Crime, Documentaries, Dramas, Fantasy, Filipino, Horror, Independent, Music & Musicals, Romance, Sci-Fi, Thriller]");

        System.out.print("Would you like to add a movie? (Enter Y or N): ");
        var condition = tuneMovie.nextLine();

        while(condition.equals("Y") || condition.equals("y")) {
            System.out.print("Enter Movie Name: ");
            var movies = tuneMovie.nextLine();

            System.out.print("Enter Category Name: ");
            var genres = tuneMovie.nextLine();

            MovieStore movie = new MovieStore(movies, genres);
            tuneMovieList.put(movie.getMovie(), movie.getGenres());


            System.out.print("Would you like to add again? (Enter Y or N): ");
            condition = tuneMovie.nextLine();

            if(condition.equals("Y") || condition.equals("y")) {
                continue;
            } else {
                System.out.println("** All New Movies **");
                System.out.println(tuneMovieList.keySet());
                System.out.println("Thank you for Adding a Movie!");
            }
        }
    }
}
