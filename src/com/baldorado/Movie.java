package com.baldorado;

public class Movie {
    private String movie;
    private String genres;

    public Movie(String newMovie, String newGenres) {
        this.movie = newMovie;
        this.genres = newGenres;
    }

    public String getMovie() {
        return this.movie;
    }

    public String getGenre() {
        return this.genres;
    }
}
