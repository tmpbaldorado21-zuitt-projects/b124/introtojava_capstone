package com.baldorado;

public class MovieStore extends Movie {

    public MovieStore(String newMovie, String newGenres) {
        super(newMovie, newGenres);
    }

    public String getMovies() {
        return super.getMovie();
    }

    public String getGenres() {
        return super.getGenre();
    }
}